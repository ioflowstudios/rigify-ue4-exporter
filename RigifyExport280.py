#                   IMPORTANT INFORMATION
# Do not import the mesh back into this file!
# This results in garbage, which is then exported along with your mesh!
# Its location -> Outliner -> Orphan Data -> Actions
#########################################################################
# Script is published under GPL
# Feel free to modify and republish.
#########################################################################
# RigifyExportTool : ver 2.0
# Environment : Blender 2.80, UnrealEditor 4.22
# Author's : ChichigeBobo and SVAY
#########################################################################
# From SVAY - I'm not a programmer, and the code can be written very badly, but you can always improve it.
#########################################################################
# From ioFlow 
# I AM a programmer but I hate python =P
# Therefore I did the bare minimum to get this script going
# (Normally I would super clean stuff before releasing it)
#########################################################################

bl_info = {
    "name": "Rigify Export Tool",
    "author": "SVAY and ChichigeBobo",
    "version": (1, 1, 1),
    "blender": (2, 80, 0),
    "location": "View3D > Rigify Export",
    "description": "Makes it easy to export Rigify skeleton for game engines",
    "warning": "",
    "wiki_url": "https://youtu.be/Kt7C5PFlg-k",
    "category": "Animation"}


import bpy, math
from bpy.types import Operator, AddonPreferences, Panel, Menu, UIList
from os import mkdir, listdir
from os.path import dirname
from bpy.props import BoolProperty, IntProperty, CollectionProperty, StringProperty
from bpy.types import Scene
from bpy.props import *

Inited = False

#=============== USER INTERFACE ===============

class RET_PT_ExportToolPanel(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_label = 'Export Tool'
    bl_context = 'objectmode'
    
    # Add UI elements
    def draw(self, context):

        scn = context.scene
        rs = bpy.context.scene

        if Inited == False:
            layout = self.layout
            row = layout.row()
            col = row.column(align=True)
            col.operator("id.init_button", text= "Init", icon ='QUIT')

        if Inited == True:

            layout = self.layout

            box = layout.box()
            box.label(text="Export Tools", icon="EXPORT")

            box2 = box.box()
            row = box2.row()
            row.label(text="Export Settings:")
            col = box2.column()
            col.prop(scn, 'FBX_Smoothing')
            col.prop(scn, 'FBX_ExportName', text= "Name")
            col.prop(scn, 'isWithoutFacialBones', text = "Without Face Bones")
            col.prop(scn, 'RenameBonesForMan')
            col.prop(scn, 'DeleteDEFPrefix')
            col.prop(scn, 'isAnimExport')
            if scn.isAnimExport:
                col.prop(scn, 'isBakeFrameRange')
                if scn.isBakeFrameRange:
                    row = col.row()
                    row.label(text="Export Animation Frames:")
                    col = col.column(align=True)
                    col.prop(scn, 'EXPStartFrame')
                    col.prop(scn, 'EXPEndFrame')
            
            box3 = box.box()
            row = box3.row()
            row.label(text="Add Your Custom Bones:")
            rows = 0
            row = box3.row(align=True)
            row.template_list("RET_UL_BoneIngnoreList", "", scn, "custom", scn, "custom_index", rows=rows)
            col = box3.column()
            col.prop(scn, 'isCutToBone')
            col.enabled = False
            if scn.isCutToBone:
                col.prop(scn, "CuttingBoneName", text = "Bone name")

            col = row.column(align=True)
            col.operator("custom.list_action", icon='ADD', text="").action = 'ADD'
            col.operator("custom.list_action", icon='PANEL_CLOSE', text="").action = 'REMOVE'
            col.operator("id.update_button", icon ='PINNED', text="")

            box5 = box.box()
            row = box5.row()
            row.label(text="Additional tools")
            col = box5.column(align=True)
            col.operator("id.start_unrig", text = "Unrig", icon = 'UNLINKED')
            col.operator("id.start_cleanconstraints", text = "Clean constraints", icon = 'CONSTRAINT')

            box4 = box.box()
            col = box4.column()
            col.label(text="Export Folder:")
            col.prop(scn, "FBX_ExportPath", text="Path")
            row = box.column(align=True)
            row.operator("id.start_unrigify", text = "Export", icon = 'ARMATURE_DATA')
            
       
#============ END USER INTERFACE =============

DebugPrints = True

#================== SETTINGS =================

newRigName = 'root'
newActionNamePrefix = 'Anim_'
newActionNameSuffix = ''

FBX_ExportName = "UnrigifyChar"
FBX_ExportPath = ""

FBX_SmoothingType = "OFF"
FBX_Smoothing = 0
FBX_smooth_Type= "OFF"

IsBackwardRenameBones = True

#Use below if you have bones added.
nonDeleteBones = []
nonConstBones = [] #These bones' constraints get no change. In many cases, same as above.

#These bones will be checked 'deform' state.
#If it is marked, treat as deform bone. If not, do nothing.
#i.e. you should turn 'deform' on to make this effective.
#(Why don't check all deform state? Because reconstruction process requires to know the parent.)
respectDeformStateBones = [ # bone name, parent bone, connected (this is mainly for Constrant of PhysicsAsset)
          ['lEye', 'DEF-spine.006', False],
          ['rEye', 'DEF-spine.006', False],
          ['upperTeeth',   'DEF-spine.006', False],
          ['lowerTeeth',   'lowerJaw', False]
 ]

#============== END SETTINGS =================


#=============== CLASSES =====================

#This button call inited Function
class RET_OT_InitToolButton(bpy.types.Operator):
    """Init Addon For Work"""
    bl_idname = "id.init_button"
    bl_label = "InitButton"

    def execute (self, context):
        Register_Variables(bpy.context.scene) # Init Function
        global Inited
        Inited = True
        self.report({'INFO'}, "Inited")
        return {'FINISHED'}

# Export button
class RET_OT_startUnrigifyButtonExport(bpy.types.Operator):
    """EXPORT RIG"""
    bl_idname = "id.start_unrigify"
    bl_label = "Start Export"  

    def execute(self, context):
        if ValidateUnrig(self): #Check 
            unrigifyStart(self, context, True)
        return {'FINISHED'}

class RET_OT_startUnrigifyButton(bpy.types.Operator):
    """Detach The Rig"""
    bl_idname = "id.start_unrig"
    bl_label = "Start Unrig"  

    def execute(self, context):
        if ValidateUnrig(self): #Check 
            unrigifyStart(self, context, False)
        return {'FINISHED'}

class RET_OT_clearConstraintsButton(bpy.types.Operator):
    """Clear Constraints for this rig"""
    bl_idname = "id.start_cleanconstraints"
    bl_label = "Start Unrig"  

    def execute(self, context):          
        for obj in bpy.context.selected_objects:
            if obj.type == 'ARMATURE':
                DeleteAllConstraints(self, context,obj)
                self.report({'INFO'}, 'Rig Cleaned')
                return {'FINISHED'}
        self.report({'WARNING'}, 'Nothing selected')

# List bones for ingore delete
class RET_UL_BoneIngnoreList(UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname):
           if self.layout_type in {'DEFAULT', 'COMPACT'}:
               layout.prop(item, "name", text="", emboss=False, icon_value=icon)
           elif self.layout_type in {'GRID'}:
               pass

# Buttons for Ui list
class RET_OT_UilistActions(bpy.types.Operator):
       bl_idname = "custom.list_action"
       bl_label = "List Action"

       action = bpy.props.EnumProperty(
           items=(
               ('REMOVE', "Remove", ""),
               ('ADD', "Add", ""),
           )
       )

       def invoke(self, context, event):

           scn = context.scene
           idx = scn.custom_index

           try:
               item = scn.custom[idx]
           except IndexError:
               pass

           else:
            if self.action == 'REMOVE':
                 info = 'Item %s removed from list' % (scn.custom[scn.custom_index].name)
                 scn.custom_index -= 1
                 self.report({'INFO'}, info)
                 scn.custom.remove(idx)
            
           if self.action == 'ADD':
               item = scn.custom.add()
               item.id = len(scn.custom)
               item.name = 'Bone' #get_activeSceneObject()
               scn.custom_index = (len(scn.custom)-1)
               info = '%s added to list' % (item.name)
               self.report({'INFO'}, info)

           return {"FINISHED"}

# Veryfy bones button
class RET_OT_UpdateButton(bpy.types.Operator):
    """PRESS TO CONFIRM BONES"""
    bl_idname = "id.update_button"
    bl_label = "Update Button"
    def execute(self, context):         
        Update_ListBones(self, context.scene)
        return {'FINISHED'}

class CustomProp(bpy.types.PropertyGroup):
       '''name = StringProperty() '''
       id = IntProperty()

#=============== END CLASSES =================

#Update bone visual
def Update_VisualListBones(self, scene):
    scn = scene
    scn.custom.clear()
    scn.custom_index = (len(scn.custom)-1)
    for b in nonDeleteBones:
        item = scn.custom.add()
        item.id = len(scn.custom)
        item.name = b #get_activeSceneObject()
        scn.custom_index = (len(scn.custom)-1)
    
    info = '%s Bones in List' % (len(nonDeleteBones))
    self.report({'INFO'}, info)

#Update bone in List
def Update_ListBones(self, scene):
    scn = scene
    nonDeleteBones.clear()
    for i in scn.custom:
        nonDeleteBones.append(i.name)
        #self.report({'INFO'}, i.name)
    Update_VisualListBones(self, scn)

def FBX_Smoothing_Selector_Callback (scene,context):
    
    global FBX_SmoothingType
    FBX_SmoothingType = scene.FBX_Smoothing
    print (str(FBX_SmoothingType))

# Grab values from Path
def Get_Export_Path(label, key, scn):        
    global FBX_ExportPath     
    FBX_ExportPath = scn["FBX_ExportPath"] 

def Get_Export_Name(label, key, scn):        
    global FBX_ExportName    
    FBX_ExportName = scn["FBX_ExportName"]

#Registration Vars
def Register_Variables(scn):
     #Smoothing items
    FBX_smooth_Type = [
                    ('OFF', 'OFF', 'OFF'),
                    ('FACE', 'FACE', 'FACE'),
                    ('EDGE', 'EDGE' ,'EDGE')
                    ]
    
    #Smoothing
    bpy.types.Scene.FBX_Smoothing = bpy.props.EnumProperty(
        items = FBX_smooth_Type,                      
        name = "FBX Smooth type used:",
        default = 'OFF',
        update = FBX_Smoothing_Selector_Callback,
        description='Smoothing type for the objects')
    scn['FBX_Smoothing'] = 0   

    #Custom name
    bpy.types.Scene.FBX_ExportName = bpy.props.StringProperty(
        name = "FBX Custom Name",
        default='', #New Name
        description='Export Objects with a custom name')
    scn['FBX_ExportName'] = "UnrigifyChar"

    #Custom Path
    bpy.types.Scene.FBX_ExportPath = bpy.props.StringProperty(
        name = "FBX Custom Folder",
        default='X:\\library\\_media\\3D\\daz\\characters\\asos\\blonde01\\Exports\\', #Custom Export Folder
        description='Export Objects To a custom Path',
        subtype = 'DIR_PATH')
    scn['FBX_ExportPath'] = 'X:\\library\\_media\\3D\\daz\\characters\\asos\\blonde01\\Exports\\'

    bpy.types.Scene.custom = bpy.props.CollectionProperty(name = "BoneList",type=CustomProp)

    bpy.types.Scene.custom_index = bpy.props.IntProperty(name = "Cust Index")

    bpy.types.Scene.RenameBonesForMan = bpy.props.BoolProperty(name = "Rename For Mannequin")

    bpy.types.Scene.DeleteDEFPrefix = bpy.props.BoolProperty(name = "Delete DEF Prefix")

    #Baking frame range.
    #If False, the range is calculated from Action and NLA.
    #If True, use your StartFrame and EndFrame.
    bpy.types.Scene.isBakeFrameRange = bpy.props.BoolProperty(name = "Bake Frame Range", default = False)

    bpy.types.Scene.isWithoutFacialBones = bpy.props.BoolProperty(name = "Export Without Face Bones", default = False)

    bpy.types.Scene.isAnimExport = bpy.props.BoolProperty(name = "Export Animation", default = True)

    bpy.types.Scene.isCutToBone = bpy.props.BoolProperty(name = "Cut To The Bone", default = False, description='Cuts off part of the skeleton bone, leaving her descendants')

    bpy.types.Scene.CuttingBoneName = bpy.props.StringProperty(name = "Cutting Bone Name")

    bpy.types.Scene.EXPStartFrame = bpy.props.IntProperty(name = "Start", min = 0, default= 0)

    bpy.types.Scene.EXPEndFrame = bpy.props.IntProperty(name = "End", min = 0, default= 100)

# Export Function
def FBX_Export(self, context):

    global FBX_ExportName
    global FBX_ExportPath

    scn = context.scene

    #Get_Export_Path("String:   ", 'FBX_ExportPath', scn)
    #Get_Export_Name("String:   ", 'FBX_ExportPath', scn)

    bpy.ops.export_scene.fbx(check_existing=True,
                             filepath= FBX_ExportPath + '/'+ FBX_ExportName +'.fbx',
                             filter_glob="*.fbx",
                             use_selection=True, 
                             apply_unit_scale=True,
                             axis_forward='-Z',
                             axis_up='Y',
                             add_leaf_bones=False,
                             bake_space_transform=True,
                             object_types= {'MESH', 'ARMATURE'},
                             use_mesh_modifiers=True,
                             mesh_smooth_type=FBX_SmoothingType, 
                             use_mesh_edges=False, 
                             use_tspace=False,
                             use_armature_deform_only=False,
                             bake_anim=scn.isAnimExport,
                             bake_anim_use_all_bones=True,
                             bake_anim_use_all_actions=True, #Tatatata
                             use_custom_props=True,                             
                             path_mode='AUTO',
                             embed_textures=False, 
                             batch_mode='OFF', 
                             use_batch_own_dir=False,
                             use_metadata=True)   

#============= UNRIGIFY LOGIC ================ Thank ChichigeBobo 

# Basic function to unrig
def unrigifyStart(self,context,export):
    scn = context.scene
    
    Get_Export_Path("String:   ", 'FBX_ExportPath', scn)
    Get_Export_Name("String:   ", 'FBX_ExportPath', scn)
    
    
    if FBX_ExportPath == '':
     self.report({'WARNING'}, "Specify the export path")
     self.report({'INFO'}, FBX_ExportPath)
     return
    
    initialSelection = bpy.context.selected_objects        
    for obj in initialSelection:
        if obj.type == 'ARMATURE':
            #bpy.context.object = obj
            bpy.context.view_layer.objects.active = obj 
            rigifyObj = obj
        else:
            #obj.select = False
            obj.select_set(False)

    if scn.isCutToBone:
        scn.cursor_location = rigifyObj.data.bones.get(('DEF-'+scn.CuttingBoneName)).head
    
    boneNames = []
    if scn.isCutToBone:
        boneNames = getCutModeBoneNames(self,scn, rigifyObj)
    else:
        if len(rigifyObj.data.bones) > 520:
            boneNames = getPitchiPoyBoneNames(scn, self,rigifyObj)  
        elif len(rigifyObj.data.bones) > 360:
            boneNames = getBoneNames(self,rigifyObj)
        else:
            boneNames = getFaceBoneNames(self,rigifyObj)
        
    if DebugPrints:
        self.report({'WARNING'}, '%s bones in rig' % (len(rigifyObj.data.bones)))
    
    bpy.ops.object.mode_set(mode = 'OBJECT')
    bpy.ops.object.duplicate_move(OBJECT_OT_duplicate={"linked":False}) #'invalid context' in console. I don't know how to fix

    newRigObj = bpy.context.object
    amt = newRigObj.data

    # if scn.isCutToBone:
    #     newRigName = scn.CuttingBoneName
    global newRigName
    newRigObj.name = newRigName
    newRigObj.name = newRigName #prevent '.001'
    newRigObj.animation_data.action = None
    #newRigObj.pose.bones['DEF-spine'].custom_shape = None
    #amt.layers = [i in [28, 29] for i in range(32)]
    amt.layers =  [True for i in range(32)] # all
    
    #------- merge effective 'respectDeformStateBones' items to main bone names -------
    if not scn.isWithoutFacialBones:
        temp = []
        for b in rigifyObj.data.bones:
            for rdb in respectDeformStateBones:
                if b.name == rdb[0] and b.use_deform:
                    rdb.append(rdb[0]) #bone name for mannequin
                    temp.append(rdb)
                    break
        boneNames.extend(temp)
              
    #------- delete bones but deforms ---------
    bpy.ops.object.mode_set(mode = 'EDIT')
    deleteBones = []
    for eb in amt.edit_bones:
        isFound = False
        for bn in boneNames:
            if eb.name == bn[0]:
                isFound = True
                break
        if not isFound:
            #print(eb.name)
            deleteBones.append(eb)
     
    for db in deleteBones:
        #self.report({'WARNING'}, 'deleting bone %s' % db)
        amt.edit_bones.remove(db)
    
    #------ set hierarchy -----------
    for b in amt.edit_bones:
        for bn in boneNames:
            if b.name == bn[0] and bn[1] != None:
                isFound = True
                b.parent = amt.edit_bones[bn[1]]
                b.use_connect = bn[2]
                b.use_inherit_rotation = False  #works fine w/o these. 
                b.use_inherit_scale = False     #otherwise, I don't know why work w/o these.
                if not bn[2]:
                    b.use_local_location = False
                break
        
    #------ set constraints (and unlock)---------- 
    bpy.ops.object.mode_set(mode = 'OBJECT')
    
    for b in newRigObj.pose.bones: 
        if b.name in nonConstBones:
            continue
        
        for c in b.constraints:
            b.constraints.remove(c)

        cnst = b.constraints.new('COPY_LOCATION')
        cnst.name = 'Copy Location'
        cnst.target = rigifyObj
        cnst.subtarget = b.name    
        
        cnst = b.constraints.new('COPY_ROTATION')
        cnst.name = 'Copy Rotation'
        cnst.target = rigifyObj
        cnst.subtarget = b.name    

        cnst = b.constraints.new('COPY_SCALE')
        cnst.name = 'Copy Scale'
        cnst.target = rigifyObj
        cnst.subtarget = b.name
        cnst.target_space = 'WORLD' #seems better than 'POSE'
        cnst.owner_space = 'POSE'
        
        #-- COPY_TRANSFORMS didn't work. Seems that loc and rot need World coord.
        
        b.lock_location = [False, False, False]
        b.lock_rotation = [False, False, False]
        b.lock_rotation_w = False
        b.lock_rotations_4d = False
        b.lock_scale = [False, False, False]
    
    #-------- size manipulation --------             
   
    if scn.isCutToBone:
        newRigObj.select_set(True)
        bpy.ops.object.origin_set(type='ORIGIN_CURSOR',center='MEDIAN')
        #newRigObj.location = (0,0,0)
        for obj in initialSelection:
            if obj.type == 'MESH':
                obj.select_set(True)
                bpy.ops.object.origin_set(type='ORIGIN_CURSOR',center='MEDIAN')
                obj.location = (0,0,0)
    else:
        empty = bpy.data.objects.new('Empty', None)
        empty.location =  (0,0,0) #newRigObj.location
        #bpy.context.scene.objects.link(empty)
        bpy.context.collection.objects.link(empty)
        newRigObj.scale = (100, 100, 100)
        newRigObj.parent = empty
        empty.scale = (0.01, 0.01, 0.01)
        
    #newRigObj.select = True
    newRigObj.select_set(True)

    #newRigObj.data.bones.get(('DEF-' + scn.CuttingBoneName)).head = (0,0,0)

    bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)
    
    #--------- baking ---------------
    frameRange = getFrameRange(rigifyObj) #return None if animation doesn't exist. 
    if frameRange and scn.isBakeFrameRange:
        frameRange = [scn.EXPStartFrame, scn.EXPEndFrame]
    
    if frameRange:
        bpy.ops.nla.bake(frame_start=frameRange[0], frame_end=frameRange[1], step=1, only_selected=False, visual_keying=True, clear_constraints=True, bake_types={'POSE'})
        #bpy.ops.action.clean() #invalid context error.
        rigifyAction = rigifyObj.animation_data.action
        newAction = newRigObj.animation_data.action
        if rigifyAction:
            newAction.name = newActionNamePrefix + rigifyAction.name + newActionNameSuffix
            newAction.name = newActionNamePrefix + rigifyAction.name + newActionNameSuffix #prevent .001
        else:
            trackName = rigifyObj.animation_data.nla_tracks[0].name
            newAction.name = newActionNamePrefix + trackName + newActionNameSuffix
            newAction.name = newActionNamePrefix + trackName + newActionNameSuffix #prevent .001
        
        tracks = newRigObj.animation_data.nla_tracks
        if tracks:
            tracksCopy = tracks[:]
            for tc in tracksCopy:
                newRigObj.animation_data.nla_tracks.remove(tc)  
         
    #----- point armature modifier to new rig ----
    for obj in initialSelection:
        if obj.type == 'ARMATURE':
            obj.select_set(False)
        elif obj.type == 'MESH':
            for m in obj.modifiers:
                if m.type == 'ARMATURE' and m.object == rigifyObj:
                    m.object = newRigObj
            obj.select_set(True)
        else:
            obj.select_set(False)    
    
    #bpy.context.scene.objects.active = newRigObj
    bpy.context.view_layer.objects.active = newRigObj 

    # Safe before Export
    bpy.ops.ed.undo_push()

    if scn.RenameBonesForMan:
        startRenameBonesAndVertGroup(self)
    
    if scn.DeleteDEFPrefix:
        startRenameDEFPrefix(self)
        
    if export:
        # Export Rig to FBX
        FBX_Export(self, context)
    
        # Undo changes
        bpy.ops.ed.undo()

        newRigName = 'root'
        self.report({'INFO'}, 'Exported')
        return
    
    
def getBoneNames(self,rigifyArm):
    # [subject, parent, connected]
    if DebugPrints:
        self.report({'WARNING'}, 'CALL: getBoneNames')
    boneNames = [
              ['DEF-spine',     None,          False],
              ['DEF-spine.001', 'DEF-spine',     True],
              ['DEF-spine.002', 'DEF-spine.001', True],
              ['DEF-spine.003', 'DEF-spine.002', True],
              ['DEF-spine.004', 'DEF-spine.003', True],
              ['DEF-spine.005', 'DEF-spine.004', True],
              ['DEF-spine.006', 'DEF-spine.005', True],
			  ['DEF-spine.007', 'DEF-spine.006', True],
                            
              ['DEF-pelvis.L',   'DEF-spine',     False],
			  ['DEF-pelvis.R',   'DEF-spine',     False],

              ['DEF-breast.L',   'DEF-spine.003', False],
			  ['DEF-breast.R',   'DEF-spine.003', False],

              ['DEF-shoulder.L', 'DEF-spine.003', False],
			  ['DEF-shoulder.R', 'DEF-spine.003', False],
              
              ['DEF-upper_arm.L',     'DEF-shoulder.L',      False],
              ['DEF-upper_arm.L.001', 'DEF-upper_arm.L',     True],
              ['DEF-forearm.L',       'DEF-upper_arm.L.001', True],
              ['DEF-forearm.L.001',   'DEF-forearm.L',       True],
              ['DEF-hand.L',          'DEF-forearm.L.001',   True],
              
              ['DEF-palm.01.L',  'DEF-hand.L',     False],
              ['DEF-palm.02.L',  'DEF-hand.L',     False], 
              ['DEF-palm.03.L',  'DEF-hand.L',     False],
              ['DEF-palm.04.L',  'DEF-hand.L',     False], 
              
              ['DEF-thumb.01.L', 'DEF-palm.01.L',  False],
              ['DEF-thumb.02.L', 'DEF-thumb.01.L', True], 
              ['DEF-thumb.03.L', 'DEF-thumb.02.L', True],
              
              ['DEF-f_index.01.L',  'DEF-palm.01.L',     False],
              ['DEF-f_index.02.L',  'DEF-f_index.01.L',  True],
              ['DEF-f_index.03.L',  'DEF-f_index.02.L',  True],
              
              ['DEF-f_middle.01.L', 'DEF-palm.02.L',     False],
              ['DEF-f_middle.02.L', 'DEF-f_middle.01.L', True],
              ['DEF-f_middle.03.L', 'DEF-f_middle.02.L', True],
              
              ['DEF-f_ring.01.L',   'DEF-palm.03.L',     False],
              ['DEF-f_ring.02.L',   'DEF-f_ring.01.L',   True],
              ['DEF-f_ring.03.L',   'DEF-f_ring.02.L',   True],
              
              ['DEF-f_pinky.01.L',  'DEF-palm.04.L',     False],
              ['DEF-f_pinky.02.L',  'DEF-f_pinky.01.L',  True],
              ['DEF-f_pinky.03.L',  'DEF-f_pinky.02.L',  True],
			  
			  ['DEF-upper_arm.R',     'DEF-shoulder.R',      False],
              ['DEF-upper_arm.R.001', 'DEF-upper_arm.R',     True],
              ['DEF-forearm.R',       'DEF-upper_arm.R.001', True],
              ['DEF-forearm.R.001',   'DEF-forearm.R',       True],
              ['DEF-hand.R',          'DEF-forearm.R.001',   True],
              
              ['DEF-palm.01.R',  'DEF-hand.R',     False],
              ['DEF-palm.02.R',  'DEF-hand.R',     False], 
              ['DEF-palm.03.R',  'DEF-hand.R',     False],
              ['DEF-palm.04.R',  'DEF-hand.R',     False], 
              
              ['DEF-thumb.01.R', 'DEF-palm.01.R',  False],
              ['DEF-thumb.02.R', 'DEF-thumb.01.R', True], 
              ['DEF-thumb.03.R', 'DEF-thumb.02.R', True],
              
              ['DEF-f_index.01.R',  'DEF-palm.01.R',     False],
              ['DEF-f_index.02.R',  'DEF-f_index.01.R',  True],
              ['DEF-f_index.03.R',  'DEF-f_index.02.R',  True],
              
              ['DEF-f_middle.01.R', 'DEF-palm.02.R',     False],
              ['DEF-f_middle.02.R', 'DEF-f_middle.01.R', True],
              ['DEF-f_middle.03.R', 'DEF-f_middle.02.R', True],
              
              ['DEF-f_ring.01.R',   'DEF-palm.03.R',     False],
              ['DEF-f_ring.02.R',   'DEF-f_ring.01.R',   True],
              ['DEF-f_ring.03.R',   'DEF-f_ring.02.R',   True],
              
              ['DEF-f_pinky.01.R',  'DEF-palm.04.R',     False],
              ['DEF-f_pinky.02.R',  'DEF-f_pinky.01.R',  True],
              ['DEF-f_pinky.03.R',  'DEF-f_pinky.02.R',  True],
              
              ['DEF-thigh.L',     'DEF-spine',       False],
              ['DEF-thigh.L.001', 'DEF-thigh.L',     True],
              ['DEF-shin.L',      'DEF-thigh.L.001', True],
              ['DEF-shin.L.001',  'DEF-shin.L',      True],
              ['DEF-foot.L',      'DEF-shin.L.001',  True],
              ['DEF-toe.L',       'DEF-foot.L',      True],
			  
			  ['DEF-thigh.R',     'DEF-spine',       False],
              ['DEF-thigh.R.001', 'DEF-thigh.R',     True],
              ['DEF-shin.R',      'DEF-thigh.R.001', True],
              ['DEF-shin.R.001',  'DEF-shin.R',      True],
              ['DEF-foot.R',      'DEF-shin.R.001',  True],
              ['DEF-toe.R',       'DEF-foot.R',      True]]

    #adding flipped bones
    ref = boneNames[:]
    for bn in ref:
        if bn[0].endswith(('.L', '.L.001', '.L.002')):
            if bn[0].endswith('.L'):
                flipped = bn[0].rsplit('.L', 1)[0] + '.R'
            elif bn[0].endswith('.L.01'):
                flipped = bn[0].rsplit('L.001', 1)[0] + 'R.001'
            else:
                flipped = bn[0].rsplit('L.002', 1)[0] + 'R.002'
                        
            if bn[1].endswith(('.L', '.L.01', '.L.02')):
                if bn[1].endswith('.L'):
                    flippedPnt = bn[1].rsplit('.L', 1)[0] + '.R'
                elif bn[1].endswith('.L.01'):
                    flippedPnt = bn[1].rsplit('L.01', 1)[0] + 'R.01'
                else:
                    flippedPnt = bn[1].rsplit('L.02', 1)[0] + 'R.02'
            else:
                flippedPnt = bn[1]
                        
            #boneNames.append([flipped, flippedPnt, bn[2]])
            #self.report({'WARNING'}, 'getBoneNames: appending %s, %s, %s' %(flipped, flippedPnt, bn[2]))
    
     # Add Custom Bones to BoneNames
    for bone in nonDeleteBones:
        BoneName = 'DEF-' + bone
        if BoneName in rigifyArm.data.bones: # Check contains
            OrigParNam = getattr(rigifyArm.data.bones.get(BoneName).parent, "name", "None") # Get Parent Name

            if OrigParNam == 'root': # Check parent
                #self.report({'ERROR'}, 'BONE NOT PARENT: %s'%(bone))
                boneNames.append([BoneName, 'DEF-spine', False])
            else:
                ParrentName = ('DEF-' + (OrigParNam.split('-')[1])) # Rename parent, it may be incorrect by default.
                UseConnect = rigifyArm.data.bones.get(BoneName).use_connect # Get Connect flag for bone (my bad English sorry)

                #info = '%s, %s, %s\n' %(BoneName, ParrentName, UseConnect)
                #self.report({'INFO'}, info)
                boneNames.append([BoneName, ParrentName, UseConnect])

    return boneNames


def getCutModeBoneNames(self,scn,rigifyArm):

    if DebugPrints:
        self.report({'WARNING'}, 'CALL: getCutModeBoneNames')

    boneNames = []

    BoneName = 'DEF-' + scn.CuttingBoneName

    if BoneName in rigifyArm.data.bones: # Check contains
        fillRecursiveCutBoneNames(self, scn, rigifyArm, BoneName, boneNames)
        if DebugPrints:
            for p in boneNames:
                self.report({'WARNING'}, '%s , %s , %s' % (p[0],p[1],p[2]))  
        return boneNames    
    else:
        self.report({'WARNING'}, 'Cut bone nothing in Armature')


def fillRecursiveCutBoneNames(self,scn,rigifyArm,BoneName, boneNames):
    if 'DEF-' in BoneName:
        ParentName = getParentForBone(scn, self, BoneName, rigifyArm)
        UseConnect = rigifyArm.data.bones.get(BoneName).use_connect

        if BoneName == ('DEF-' + scn.CuttingBoneName):
            ParentName = None
            UseConnect = False

        if ParentName == ('DEF-' + scn.CuttingBoneName):
            ParentName = None
            UseConnect = False

        if BoneName != ('DEF-' + scn.CuttingBoneName):
            boneNames.append([BoneName, ParentName, UseConnect])

        #boneNames.append([BoneName, ParentName, UseConnect])
        
    if len(rigifyArm.data.bones.get(BoneName).children) > 0:
        for b in rigifyArm.data.bones.get(BoneName).children:
            fillRecursiveCutBoneNames(self, scn, rigifyArm, b.name, boneNames)
        
def getParentForBone(scn ,self, BoneName, rigifyArm):

    boneNames = getPitchiPoyBoneNames(scn, self,rigifyArm)

    for row in boneNames:
        if row[0] == BoneName:
            return row[1]
                
def getPitchiPoyBoneNames(scn,self,rigifyArm):
    # [subject, parent, connected, mannequin]
    boneNames = [
              ['DEF-spine',     None,          False],
              ['DEF-spine.001', 'DEF-spine',     True],
              ['DEF-spine.002', 'DEF-spine.001', True],
              ['DEF-spine.003', 'DEF-spine.002', True],
              ['DEF-spine.004', 'DEF-spine.003', True],
              ['DEF-spine.005', 'DEF-spine.004', True],
              ['DEF-spine.006', 'DEF-spine.005', True],
			  ['DEF-spine.007', 'DEF-spine.006', True],
                            
              ['DEF-pelvis.L',   'DEF-spine',     False],

              ['DEF-breast.L',   'DEF-spine.003', False],

              ['DEF-shoulder.L', 'DEF-spine.003', False],
              
              ['DEF-upper_arm.L',     'DEF-shoulder.L',      False],
              ['DEF-upper_arm.L.001', 'DEF-upper_arm.L',     True],
              ['DEF-forearm.L',       'DEF-upper_arm.L.001', True],
              ['DEF-forearm.L.001',   'DEF-forearm.L',       True],
              ['DEF-hand.L',          'DEF-forearm.L.001',   True],
              
              ['DEF-palm.01.L',  'DEF-hand.L',     False],
              ['DEF-palm.02.L',  'DEF-hand.L',     False], 
              ['DEF-palm.03.L',  'DEF-hand.L',     False],
              ['DEF-palm.04.L',  'DEF-hand.L',     False], 
              
              ['DEF-thumb.01.L', 'DEF-palm.01.L',  False],
              ['DEF-thumb.02.L', 'DEF-thumb.01.L', True], 
              ['DEF-thumb.03.L', 'DEF-thumb.02.L', True],
              
              ['DEF-f_index.01.L',  'DEF-palm.01.L',     False],
              ['DEF-f_index.02.L',  'DEF-f_index.01.L',  True],
              ['DEF-f_index.03.L',  'DEF-f_index.02.L',  True],
              
              ['DEF-f_middle.01.L', 'DEF-palm.02.L',     False],
              ['DEF-f_middle.02.L', 'DEF-f_middle.01.L', True],
              ['DEF-f_middle.03.L', 'DEF-f_middle.02.L', True],
              
              ['DEF-f_ring.01.L',   'DEF-palm.03.L',     False],
              ['DEF-f_ring.02.L',   'DEF-f_ring.01.L',   True],
              ['DEF-f_ring.03.L',   'DEF-f_ring.02.L',   True],
              
              ['DEF-f_pinky.01.L',  'DEF-palm.04.L',     False],
              ['DEF-f_pinky.02.L',  'DEF-f_pinky.01.L',  True],
              ['DEF-f_pinky.03.L',  'DEF-f_pinky.02.L',  True],
              
              ['DEF-thigh.L',     'DEF-spine',       False],
              ['DEF-thigh.L.001', 'DEF-thigh.L',     True],
              ['DEF-shin.L',      'DEF-thigh.L.001', True],
              ['DEF-shin.L.001',  'DEF-shin.L',      True],
              ['DEF-foot.L',      'DEF-shin.L.001',  True],
              ['DEF-toe.L',       'DEF-foot.L',      True]]
              
              #----- face ----
    faceBoneNames = [['CenterBrow', 'DEF-spine.006', False],
	          ['rBrowInner',     'DEF-spine.006', False],
              ['rBrowMid', 'DEF-spine.006', False],
              ['rBrowOuter', 'DEF-spine.006', False],

              ['lBrowInner',     'DEF-spine.006', False],
              ['lBrowMid', 'DEF-spine.006', False], 
              ['lBrowOuter', 'DEF-spine.006', False],
			  
              ['lEyelidInner',     'DEF-spine.006', False],
              ['lEyelidUpperInner', 'DEF-spine.006', False],
              ['lEyelidUpper', 'DEF-spine.006', False],
              ['lEyelidUpperOuter', 'DEF-spine.006', False],
              
              ['lEyelidOuter',     'DEF-spine.006', False],
              ['lEyelidLowerOuter', 'DEF-spine.006', False],
              ['lEyelidLower', 'DEF-spine.006', False],
              ['lEyelidLowerInner', 'DEF-spine.006', False],
			  
			  ['rEyelidInner',     'DEF-spine.006', False],
              ['rEyelidUpperInner', 'DEF-spine.006', False],
              ['rEyelidUpper', 'DEF-spine.006', False],
              ['rEyelidUpperOuter', 'DEF-spine.006', False],
              
              ['rEyelidOuter',     'DEF-spine.006', False],
              ['rEyelidLowerOuter', 'DEF-spine.006', False],
              ['rEyelidLower', 'DEF-spine.006', False],
              ['rEyelidLowerInner', 'DEF-spine.006', False],
			  
			  ['lEar', 'DEF-spine.006', False],
			  ['rEar', 'DEF-spine.006', False],
			  
			  ['lSquintOuter', 'DEF-spine.006', False],
			  ['lSquintInner', 'DEF-spine.006', False],
			  ['rSquintOuter', 'DEF-spine.006', False],
			  ['rSquintInner', 'DEF-spine.006', False],
			  
			  ['lCheekUpper', 'DEF-spine.006', False],
			  ['lCheekLower', 'DEF-spine.006', False],
			  ['lJawClench', 'DEF-spine.006', False],
			  
			  ['rCheekUpper', 'DEF-spine.006', False],
			  ['rCheekLower', 'DEF-spine.006', False],
			  ['rJawClench', 'DEF-spine.006', False],
			  
              ['MidNoseBridge',     'DEF-spine.006', False],
              ['Nose', 'DEF-spine.006', False],
              ['lNostril', 'DEF-spine.006', False],
              ['rNostril', 'DEF-spine.006', False],
			  
			  ['lNasolabialUpper', 'DEF-spine.006', False],
			  ['lNasolabialMiddle', 'DEF-spine.006', False],
			  ['lNasolabialLower', 'DEF-spine.006', False],
			  ['lLipNasolabialCrease', 'DEF-spine.006', False],
			  ['lLipCorner', 'DEF-spine.006', False],
			  ['lNasolabialMouthCorner', 'DEF-spine.006', False],
			  
			  ['rNasolabialUpper', 'DEF-spine.006', False],
			  ['rNasolabialMiddle', 'DEF-spine.006', False],
			  ['rNasolabialLower', 'DEF-spine.006', False],
			  ['rLipNasolabialCrease', 'DEF-spine.006', False],
			  ['rLipCorner', 'DEF-spine.006', False],
			  ['rNasolabialMouthCorner', 'DEF-spine.006', False],
			  
			  ['lLipBelowNose', 'DEF-spine.006', False],
			  ['rLipBelowNose', 'DEF-spine.006', False],
			  
			  ['LipUpperMiddle', 'DEF-spine.006', False],
			  ['LipLowerMiddle', 'DEF-spine.006', False],
			  
			  ['lLipUpperInner', 'DEF-spine.006', False],
			  ['lLipUpperOuter', 'DEF-spine.006', False],
			  ['lLipLowerOuter', 'DEF-spine.006', False],
			  ['lLipLowerInner', 'DEF-spine.006', False],
			  
			  ['rLipUpperInner', 'DEF-spine.006', False],
			  ['rLipUpperOuter', 'DEF-spine.006', False],
			  ['rLipLowerOuter', 'DEF-spine.006', False],
			  ['rLipLowerInner', 'DEF-spine.006', False],
			  
			  ['LipBelow', 'DEF-spine.006', False],
			  ['Chin', 'DEF-spine.006', False],
			  ['BelowJaw', 'DEF-spine.006', False],
			  
			  ['lowerFaceRig', 'DEF-spine.006', False],
			  ['lowerJaw', 'DEF-spine.006', False],
			  ['upperTeeth', 'DEF-spine.006', False],
			  ['lowerTeeth', 'lowerJaw', False],

              ['tongue01', 'DEF-spine.006', False],
              ['tongue02', 'DEF-spine.006', False],
              ['tongue03', 'DEF-spine.006', False],
			  ['tongue04', 'DEF-spine.006', False]]

    if scn.isWithoutFacialBones != True:
        boneNames.extend(faceBoneNames)
        if DebugPrints:
            self.report({'WARNING'}, 'Exported Without Face')
        
    #adding flipped bones
    ref = boneNames[:]
    for bn in ref:
        if bn[0].endswith('.L') or bn[0].endswith('.L.00', 0, -1): #there's L.001 to L.004
            if bn[0].endswith('.L'):
                flipped = bn[0].rsplit('.L', 1)[0] + '.R'
            else:
                temp = bn[0].rsplit('.L.00', 1)
                flipped = temp[0] + '.R.00' + temp[1]
                        
            if bn[1].endswith('.L') or bn[1].endswith('.L.00', 0, -1): 
                if bn[1].endswith('.L'):
                    flippedPnt = bn[1].rsplit('.L', 1)[0] + '.R'
                else:
                    temp = bn[1].rsplit('.L.00', 1)
                    flippedPnt = temp[0] + '.R.00' + temp[1]
            else:
                flippedPnt = bn[1]
            
            boneNames.append([flipped, flippedPnt, bn[2]])
    
    # Add Custom Bones to BoneNames
    for bone in nonDeleteBones:
        BoneName = 'DEF-' + bone
        if BoneName in rigifyArm.data.bones: # Check contains
            OrigParNam = getattr(rigifyArm.data.bones.get(BoneName).parent, "name", "None") # Get Parent Name

            if OrigParNam == 'root': # Check parent
                boneNames.append([BoneName, 'DEF-spine', False])
            else:
                ParrentName = ('DEF-' + (OrigParNam.split('-')[1])) # Rename parent, it may be incorrect by default.
                UseConnect = rigifyArm.data.bones.get(BoneName).use_connect # Get Connect flag for bone (my bad English sorry)

                #info = '%s, %s, %s\n' %(BoneName, ParrentName, UseConnect)
                #self.report({'INFO'}, info)
                boneNames.append([BoneName, ParrentName, UseConnect])

    return boneNames

def getFaceBoneNames(self, rigifyArm):
    if DebugPrints:
        self.report({'WARNING'}, 'CALL: getFaceBoneNames')
    boneNames = [
                ['Face', None, False],
              ['DEF-brow.B.L',     'Face', False], #brow.B and lid are (indirectly) parented to master_eye.L
              ['DEF-brow.B.L.001', 'Face', False], 
              ['DEF-brow.B.L.002', 'Face', False],
              ['DEF-brow.B.L.003', 'Face', False],

              ['DEF-lid.T.L',     'Face', False],
              ['DEF-lid.T.L.001', 'Face', False],
              ['DEF-lid.T.L.002', 'Face', False],
              ['DEF-lid.T.L.003', 'Face', False],
              
              ['DEF-lid.B.L',     'Face', False],
              ['DEF-lid.B.L.001', 'Face', False],
              ['DEF-lid.B.L.002', 'Face', False],
              ['DEF-lid.B.L.003', 'Face', False],
              
              ['DEF-nose',     'Face', False],
              ['DEF-nose.001', 'Face', False],
              ['DEF-nose.002', 'Face', False],
              ['DEF-nose.003', 'Face', False],
              ['DEF-nose.004', 'Face', False],

              ['DEF-nose.L',     'Face', False],
              ['DEF-nose.L.001', 'Face', False],

              ['DEF-cheek.T.L',     'Face', False],
              ['DEF-cheek.T.L.001', 'Face', False],
              ['DEF-cheek.B.L',     'Face', False],
              ['DEF-cheek.B.L.001', 'Face', False],
              
              ['DEF-lip.T.L',     'Face', False],
              ['DEF-lip.T.L.001', 'Face', False],
              ['DEF-lip.B.L',     'Face', False],
              ['DEF-lip.B.L.001', 'Face', False],

              ['DEF-tongue',     'Face', False],
              ['DEF-tongue.001', 'Face', False],
              ['DEF-tongue.002', 'Face', False],
              
              ['DEF-chin',      'Face', False],
              ['DEF-chin.001',  'Face', False],

              ['DEF-chin.L',    'Face', False],
              
              ['DEF-jaw',       'Face', False],

              ['DEF-jaw.L',     'Face', False],
              ['DEF-jaw.L.001', 'Face', False],
              
              ['DEF-temple.L',  'Face', False],

              ['DEF-ear.L',     'Face', False],
              ['DEF-ear.L.001', 'Face', False],
              ['DEF-ear.L.002', 'Face', False],
              ['DEF-ear.L.003', 'Face', False],
              ['DEF-ear.L.004', 'Face', False]]

    #adding flipped bones
    ref = boneNames[:]
    for bn in ref:
        if bn[0].endswith('.L') or bn[0].endswith('.L.00', 0, -1): #there's L.001 to L.004
            if bn[0].endswith('.L'):
                flipped = bn[0].rsplit('.L', 1)[0] + '.R'
            else:
                temp = bn[0].rsplit('.L.00', 1)
                flipped = temp[0] + '.R.00' + temp[1]
                        
            if bn[1].endswith('.L') or bn[1].endswith('.L.00', 0, -1): 
                if bn[1].endswith('.L'):
                    flippedPnt = bn[1].rsplit('.L', 1)[0] + '.R'
                else:
                    temp = bn[1].rsplit('.L.00', 1)
                    flippedPnt = temp[0] + '.R.00' + temp[1]
            else:
                flippedPnt = bn[1]
            
            boneNames.append([flipped, flippedPnt, bn[2]])
    
    # Add Custom Bones to BoneNames
    for bone in nonDeleteBones:
        BoneName = 'DEF-' + bone
        if BoneName in rigifyArm.data.bones: # Check contains
            OrigParNam = getattr(rigifyArm.data.bones.get(BoneName).parent, "name", "None") # Get Parent Name

            if OrigParNam == 'root': # Check parent
                #self.report({'ERROR'}, 'BONE NOT PARENT: %s'%(bone))
                boneNames.append([BoneName, 'Face', False])
            else:
                ParrentName = ('DEF-' + (OrigParNam.split('-')[1])) # Rename parent, it may be incorrect by default.
                UseConnect = rigifyArm.data.bones.get(BoneName).use_connect # Get Connect flag for bone (my bad English sorry)

                #info = '%s, %s, %s\n' %(BoneName, ParrentName, UseConnect)
                #self.report({'INFO'}, info)
                boneNames.append([BoneName, ParrentName, UseConnect])

    return boneNames

def getMannequinBoneNames():
    # [PitchiPoy, Mannequin]
    boneNames = [
              ['DEF-spine',     'pelvis'],
              ['DEF-spine.001', 'spine_01'],
              ['DEF-spine.002', 'spine_02'],
              ['DEF-spine.003', 'spine_03'],
              ['DEF-spine.004', 'neck_01'],
              ['DEF-spine.006', 'head'],
			  ['DEF-spine.007', 'crown'],
                            
              ['DEF-shoulder.L', 'clavicle_l'],
              
              ['DEF-upper_arm.L',     'upperarm_l'],
              ['DEF-upper_arm.L.001', 'upperarm_twist_01_l'],
              ['DEF-forearm.L',       'lowerarm_l'],
              ['DEF-forearm.L.001',   'lowerarm_twist_01_l'],
              ['DEF-hand.L',          'hand_l'],
              
              ['DEF-thumb.01.L', 'thumb_01_l'],
              ['DEF-thumb.02.L', 'thumb_02_l'], 
              ['DEF-thumb.03.L', 'thumb_03_l'],
              
              ['DEF-f_index.01.L',  'index_01_l'],
              ['DEF-f_index.02.L',  'index_02_l'],
              ['DEF-f_index.03.L',  'index_03_l'],
              
              ['DEF-f_middle.01.L', 'middle_01_l'],
              ['DEF-f_middle.02.L', 'middle_02_l'],
              ['DEF-f_middle.03.L', 'middle_03_l'],
              
              ['DEF-f_ring.01.L',   'ring_01_l'],
              ['DEF-f_ring.02.L',   'ring_02_l'],
              ['DEF-f_ring.03.L',   'ring_03_l'],
              
              ['DEF-f_pinky.01.L',  'pinky_01_l'],
              ['DEF-f_pinky.02.L',  'pinky_02_l'],
              ['DEF-f_pinky.03.L',  'pinky_03_l'],
              
              ['DEF-thigh.L',     'thigh_l'],
              ['DEF-thigh.L.001', 'thigh_twist_01_l'],
              ['DEF-shin.L',      'calf_l'],
              ['DEF-shin.L.001',  'calf_twist_01_l'],
              ['DEF-foot.L',      'foot_l'],
              ['DEF-toe.L',       'ball_l'],
			  
  			  ['DEF-shoulder.R', 'clavicle_r'],
			  
  			  ['DEF-upper_arm.R',     'upperarm_r'],
              ['DEF-upper_arm.R.001', 'upperarm_twist_01_r'],
              ['DEF-forearm.R',       'lowerarm_r'],
              ['DEF-forearm.R.001',   'lowerarm_twist_01_r'],
              ['DEF-hand.R',          'hand_r'],
              
			  ['DEF-thumb.01.R', 'thumb_01_r'],
              ['DEF-thumb.02.R', 'thumb_02_r'], 
              ['DEF-thumb.03.R', 'thumb_03_r'],
              
              ['DEF-f_index.01.R',  'index_01_r'],
              ['DEF-f_index.02.R',  'index_02_r'],
              ['DEF-f_index.03.R',  'index_03_r'],
              
              ['DEF-f_middle.01.R', 'middle_01_r'],
              ['DEF-f_middle.02.R', 'middle_02_r'],
              ['DEF-f_middle.03.R', 'middle_03_r'],
              
              ['DEF-f_ring.01.R',   'ring_01_r'],
              ['DEF-f_ring.02.R',   'ring_02_r'],
              ['DEF-f_ring.03.R',   'ring_03_r'],
              
              ['DEF-f_pinky.01.R',  'pinky_01_r'],
              ['DEF-f_pinky.02.R',  'pinky_02_r'],
              ['DEF-f_pinky.03.R',  'pinky_03_r'],

              ['DEF-thigh.R',     'thigh_r'],
              ['DEF-thigh.R.001', 'thigh_twist_01_r'],
              ['DEF-shin.R',      'calf_r'],
              ['DEF-shin.R.001',  'calf_twist_01_r'],
              ['DEF-foot.R',      'foot_r'],
              ['DEF-toe.R',       'ball_r']]

    #adding flipped bones
    ref = boneNames[:]
    for bn in ref:
        if bn[0].endswith('.L') or bn[0].endswith('.L.00', 0, -1): #there's L.001 to L.004
            if bn[0].endswith('.L'):
                flipped = bn[0].rsplit('.L', 1)[0] + '.R'
            else:
                temp = bn[0].rsplit('.L.00', 1)
                flipped = temp[0] + '.R.00' + temp[1]
            
            #where rigify has left bone, mannequin too.
            mqFlipped = bn[1].rsplit('_l', 1)[0] + '_r'
            
            #boneNames.append([flipped, mqFlipped])
    
    return boneNames

def startRenameDEFPrefix(self):
    for obj in bpy.context.selected_objects:
        
        if obj.type == 'ARMATURE':
            #bpy.context.scene.objects.active = obj
            bpy.context.view_layer.objects.active = obj
            bpy.ops.object.mode_set(mode = 'EDIT')
            for b in obj.data.edit_bones:
                if 'DEF-' in b.name:
                    b.name = (b.name.split('-')[1])
            bpy.ops.object.mode_set(mode = 'OBJECT')    
        
        elif obj.type == 'MESH':
            for vg in obj.vertex_groups:
                if 'DEF-' in vg.name:
                    vg.name = (vg.name.split('-')[1])

def startRenameBonesAndVertGroup(self):

    if ValidateRenameBones(self) == False:
        self.report({'WARNING'}, 'Not validated')
        return

    boneNames = getMannequinBoneNames()

    changedBones = 0
    changedGroups = 0
            
    for obj in bpy.context.selected_objects:
        
        if obj.type == 'ARMATURE':
            #bpy.context.scene.objects.active = obj
            bpy.context.view_layer.objects.active = obj 
            bpy.ops.object.mode_set(mode = 'EDIT')
            for b in obj.data.edit_bones:
                for bn in boneNames:
                    if b.name == (bn[1] if IsBackwardRenameBones else bn[0]):
                        b.name = bn[0] if IsBackwardRenameBones else bn[1]
                        changedBones += 1
                        break
            bpy.ops.object.mode_set(mode = 'OBJECT')    
        
        elif obj.type == 'MESH':
            for vg in obj.vertex_groups:
                for bn in boneNames:
                    if vg.name == (bn[1] if IsBackwardRenameBones else bn[0]):                            
                        vg.name = bn[0] if IsBackwardRenameBones else bn[1]
                        changedGroups += 1 # BUG? when executed with an armature and a mesh selected, this line is not run. Though the line above seems executed.
                        break

#If no animation found, returns None. Fcurve's mute is not checked.
def getFrameRange(obj):
    ad = obj.animation_data
    if ad:
        min = None
        max = None

        if ad.action:
            min, max = ad.action.frame_range
            
        isActionHampersBefore = (ad.action and ad.action_blend_type == 'REPLACE' and ad.action_extrapolation == 'HOLD')
        isActionHampersAfter = (ad.action and ad.action_blend_type == 'REPLACE' and ad.action_extrapolation != 'NOTHING')
        
        if ad.use_nla: #speaker icon
            
            for track in ad.nla_tracks:
                if not track.mute:
                    
                    for strip in track.strips:
                        if not min: #there's no Action
                            min = strip.frame_start
                            max = strip.frame_end
                        else:
                            if min > strip.frame_start and not isActionHampersBefore:
                                min = strip.frame_start
                            if max < strip.frame_end and not isActionHampersAfter:
                                max = strip.frame_end
        
        if min is None: #'not min' is not adequate. Because if an action starts at frame 0, it is treated as None. 
            return None
        else:
            min = math.ceil(min)
            max = math.ceil(max)
            return [min, max]
    
    else:
        return None

def ValidateUnrig(self):
    if len(bpy.context.selected_objects) == 0:
        self.report({'WARNING'}, "Nothing selected")
        return False
    
    selectObjs = bpy.context.selected_objects
    armatureCount = 0
    rigifyObj = None
    for obj in selectObjs:
        if obj.type == 'ARMATURE':
            rigifyObj = obj
            armatureCount += 1
    
    if armatureCount == 0:
        self.report({'WARNING'}, "No armature is selected")
        return False
    elif armatureCount >= 2:
        self.report({'WARNING'}, "Multiple armatures are selected")
        return False
    
    #-----check rigify version----------
    #Standard rigify: 431 bones, PitchiPoy rigify: 627 bones
                
    return True

def ValidateRenameBones(self):
    global IsBackwardRenameBones
    
    if len(bpy.context.selected_objects) == 0:
        self.report({'WARNING'}, 'Nothing selected')
        return False
    
    selectObjs = bpy.context.selected_objects
    armatureCount = 0
    meshCount = 0
    rigifyObj = None
    for obj in selectObjs:
        if obj.type == 'ARMATURE':
            rigifyObj = obj
            armatureCount += 1
        elif obj.type == 'MESH':
            meshCount += 1
    
    if meshCount == 0 and armatureCount == 0:
        self.report({'WARNING'}, 'No mesh and armature is selected')
        return False    
    elif armatureCount >= 2:
        self.report({'WARNING'}, 'Multiple armatures are selected')
        return False
    
    bpy.ops.object.mode_set(mode = 'OBJECT')

    mannequinCheck = ['pelvis', 'spine_01', 'spine_02', 'neck_01', 'head'] #spine_03 is skipped when converted from standard rigify
    
    #---------- detect from mesh whether forward (unrigify to mannequin) or backward --------   
    ################################
    # If you want to convert partial mesh, skip these check.
    # IsBackwardRenameBones = True/False # Don't forget to set this value when you skip this section.
    if armatureCount == 0: #no armature means there's selected mesh
        checkNames = mannequinCheck[:]
        for obj in selectObjs:
            if obj.type == 'MESH':
                for vg in obj.vertex_groups:
                    if vg.name in checkNames:
                        checkNames.remove(vg.name)
                    
        if len(checkNames) == 0:
            IsBackwardRenameBones = True
        elif len(checkNames) == 5:
            IsBackwardRenameBones = False
        else:
            self.report({'WARNING'}, 'Detected vertex groups were not expected. Nothing was done')
            return False
    # skip to here.
    #################################
    
    #----------- detect whether forward or not from armature ---------- 
    if armatureCount == 1:
        checkNames = mannequinCheck[:]
        for b in rigifyObj.data.bones:
            if b.name in checkNames:
                checkNames.remove(b.name)
        if len(checkNames) == 0:
            IsBackwardRenameBones = True
        elif len(checkNames) == 5:
            IsBackwardRenameBones = False
        else:
            self.report({'WARNING'}, 'Detected vertex groups were not expected. Nothing was done')
            return False
        
        #-------- check if isPitchiPoy value is correct -------------
        #Standard rigify unrigified: 78 bones, PitchiPoy rigify unrigified: 161 bones
        if not IsBackwardRenameBones:
            if len(rigifyObj.data.bones) > 400:
                self.report({'WARNING'}, 'This script is not intended to be used on original Rigify')
                return False
            
            boneNames = getMannequinBoneNames()
            matched = 0
            for b in rigifyObj.pose.bones:
                for bn in boneNames:
                    if b.name == bn[0]:
                        matched +=  1
                        break 
                        
    return True
#=========== END UNRIGIFY LOGIC ==============

#============== FUNCTIONS ====================

def DeleteAllConstraints(self, context, rigObj):
    for b in rigObj.pose.bones: 
        for c in b.constraints:
            b.constraints.remove(c)

#============ END FUNCTIONS ==================

# Classes

classes = [
    RET_PT_ExportToolPanel,
    RET_OT_InitToolButton,
    RET_OT_startUnrigifyButtonExport,
    RET_UL_BoneIngnoreList,
    CustomProp,
    RET_OT_UilistActions,
    RET_OT_UpdateButton,
    RET_OT_startUnrigifyButton,
    RET_OT_clearConstraintsButton
]

# Register
def register():
    # Registration classes
    from bpy.utils import register_class
    for c in classes:
        bpy.utils.register_class(c)

# Unregister
def unregister():
    # Unregistration classes
    from bpy.utils import unregister_class
    for c in reversed(classes):
        bpy.utils.unregister_class(c)

    del bpy.types.Scene.custom
    del bpy.types.Scene.custom_index
    
if __name__ == '__main__':
    register()