# Rigify UE4 Exporter

This script let's you export a character in Blender 2.80 that has been rigged with the pitchipoy version of rigify into Unreal Engine 4.

I might do a tute for this soon, but the short short version is this:

1. Rigify your character
2. Select all their meshes then the rig
3. Go to the Misc tab in the N menu
4. Click Init if you haven't yet
5. Set FBX Smoothing to FACE
6. Type in a Name
7. Leave Without face bones off
8. Turn on Rename for Mannequin and Delete DEF Prefix
9. Turn off Export Animation 
10. Make sure export Folder is set
11. Click Export :)

I haven't tried exporting animation, I just use this to get daz characters out of Blender (2.80) and into UE4 (4.24)

You have to do a bunch of setup stuff in UE4 too, but there are tutes all over the web for that

It will work with all the UE4 mannequin animations but you'll have to retarget them to your characters skeleton